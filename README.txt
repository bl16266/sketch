---------------------------
Sketch Assignment Extension
---------------------------

For my extension I have added a function which draws a filled rectangle to the 
screen of a given size and colour. This function uses the opcode 7.

Rectangles are drawn using two functions, one from extendedsketch.c and one 
from extendeddisplay.c. 
Five arguments are used, starting x, starting y, width, height and rgba value.
Starting x and y use the values currently stored in s->px and s->py, width uses
the value of s->currentx and height uses the value calculated from the
instruction given in the binary file. The rgba value uses the value in s->rgba.
The rectangles are formed using the inbuilt SDL structure SDL_Rect. The
SDL_RenderFillRect is used to fill the rectangles in the given colour.

I then created three files to show the capabilities of the new function. 

The first file, extLawn.sketch attempts to recreate a mowed lawn using two
shades of green for each direction of mowing.

The second file, chess.sketch, displays a chess board.

The third file, pika.sketch, displays the pokemon Pikachu.

--------------------------
Using the extended program
--------------------------

To compile the program, simply type make in the terminal window (this will
work so long as the provided Makefile is used).

The program is used as before, type './sketch ' and then the full filename 
(file.sketch), for example './sketch pika.sketch'.

To run autotests, type './sketch' without any arguments. Automated testing 
exists for extLawn.sketch, it does not exist for the other two files however 
due to their size.

-----
Files
-----
extendedsketch.c
extendeddisplay.h
extendeddisplay.c
Makefile
extLawn.sketch
chess.sketch
pika.sketch
