#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "extendeddisplay.h"

// TODO: upgrade the run function, adding functions to support it.

typedef unsigned char byte; //unsigned char is 1 byte in size

// integer constants for opcode values
#define DX 0
#define DY 1
#define DT 2
#define PEN 3
#define CLEAR 4
#define KEY 5
#define COL 6
#define RECT 7

struct state
{
  int currentx, currenty;
  int px, py;
  bool penDown;
  display *d;
  int rgba;
};
typedef struct state State;

struct ops
{
  int opc, ope;
};
typedef struct ops Ops;

int opcode(byte b)
{
  int opc = (b >> 6);
  return opc;
}

int operand(byte b, int opc)
{
  int ope;
  if(opc == 2)
  {
    ope = b & 0x3F;
  }
  else if ((((b >> 5) & 0x1) == 1) && (b>>7 !=1))
  {
    ope = (-1 << 6) | b;
  }
  else ope = (b & 0x3F);
  return ope;
}

int operand2(byte b, int opc, int i)
{
  int ope;
  if(opc == 2 || opc == 7)
  {
    ope = b & 0xFF;
  }
  else if (i == 0)
  {
    if (b & 0x80)
    {
      ope = (-1 << 8) | b;
    }
    else ope = (b & 0xFF);
  }
  else
  {
    ope = b & 0xFF;
  }
  return ope;
}

void dx(int ope, State *s)
{
  s->currentx += ope;
}

void dy(int ope, State *s)
{
  s->currenty += ope;
  if (s->penDown)
  {
    if (s->rgba == 255)
    {
      line(s->d, s->px, s->py, s->currentx, s->currenty);
    }
    else
    {
      cline(s->d, s->px, s->py, s->currentx, s->currenty, s->rgba);
    }
  }
  s->px = s->currentx;
  s->py = s->currenty;
}

void dt(int ope, State *s)
{
  pause(s->d, ope);
}

void pen(State *s)
{
  if (s->penDown)
  {
    s->penDown = false;
  }
  else
  {
    s->penDown = true;
  }
}

void clearCom(State *s)
{
  clear(s->d);
}

void keyCom(State *s)
{
  key(s->d);
}

void col(int ope, State *s)
{
  s->rgba = ope;
}

void rectangle(int ope, State *s)
{
  int x, y, w, h;
  x = s->px;
  y = s->py;
  w = (s->currentx - s->px);
  h = ope;
  rect(s->d, x, y, w, h, s->rgba);
}

void choice1(int opc, int ope, State *s, byte b)
{
  switch(opc)
  {
    case DX:
    dx(ope, s);
    break;

    case DY:
    dy(ope, s);
    break;

    case DT:
    dt(ope, s);
    break;

    case PEN:
    pen(s);
    break;

    case CLEAR:
    clearCom(s);
    break;

    case KEY:
    keyCom(s);
    break;

    case COL:
    col(ope, s);
    break;

    case RECT:
    rectangle(ope, s);
    break;
    }
}

void extendedOp(FILE *in, int ope, State *s, byte b)
{
  ope = 0;
  int length = (b>>4) & 0x3;
  if (length == 3) length = 4;
  int opcode = b & 0xF;
  for (int i = 0; i < length; i++)
  {
    b = fgetc(in);
    ope = (ope<<8) | operand2(b, opcode, i);
  }
  choice1(opcode, ope, s, b);
}

void choice(FILE *in, int opc, int ope, State *s, byte b)
{
  switch(opc)
  {
    case DX:
    dx(ope, s);
    break;

    case DY:
    dy(ope, s);
    break;

    case DT:
    dt(ope, s);
    break;

    case 3:
    extendedOp(in, ope, s, b);
    break;

    case CLEAR:
    clearCom(s);
    break;

    case KEY:
    keyCom(s);
    break;

    case COL:
    col(ope, s);
    break;
  }
}

void interpret(FILE *in, display *d, State *s)
{
  byte b = fgetc(in);
  Ops opdata;
  Ops *o = &opdata;
  while (!feof(in))
  {
    o->opc = opcode(b);
    o->ope = operand(b, o->opc);
    choice(in, o->opc, o->ope, s, b);
    b = fgetc(in);
  }
  fclose(in);
}

// Read sketch instructions from the given file.  If test is NULL, display the
// result in a graphics window, else check the graphics calls made.
void run(char *filename, char *test[]) {
    FILE *in = fopen(filename, "rb");
    if (in == NULL) {
        fprintf(stderr, "Can't open %s\n", filename);
        exit(1);
    }
    display *d = newDisplay(filename, 240, 200, test);
    State sdata = {0, 0, 0, 0, false, d, 0x000000FF};
    State *sketcher = &sdata;
    interpret(in, d, sketcher);
    end(d);
}

// ----------------------------------------------------------------------------
// Nothing below this point needs to be changed.
// ----------------------------------------------------------------------------

// Forward declaration of test data.
char **lineTest, **squareTest, **boxTest, **oxoTest, **diagTest, **crossTest,
     **clearTest, **keyTest, **pausesTest, **fieldTest, **lawnTest, **extLawnTest;

void testSketches() {
    // Stage 1
    run("line.sketch", lineTest);
    run("square.sketch", squareTest);
    run("box.sketch", boxTest);
    run("oxo.sketch", oxoTest);

    // Stage 2
    run("diag.sketch", diagTest);
    run("cross.sketch", crossTest);

    // Stage 3
    run("clear.sketch", clearTest);
    run("key.sketch", keyTest);

    // Stage 4
    run("pauses.sketch", pausesTest);
    run("field.sketch", fieldTest);
    run("lawn.sketch", lawnTest);

    // Extension
    run("extLawn.sketch", extLawnTest);
}

int main(int n, char *args[n]) {
    if (n == 1)
    {
      testSketches();
    }
    else if (n == 2) run(args[1], NULL);
    else {
        fprintf(stderr, "Usage: sketch [file.sketch]");
        exit(1);
    }
}

// Each test is a series of calls, stored in a variable-length array of strings,
// with a NULL terminator.

// The calls that should be made for line.sketch.
char **lineTest = (char *[]) {
    "line(d,30,30,60,30)", NULL
};

// The calls that should be made for square.sketch.
char **squareTest = (char *[]) {
    "line(d,30,30,60,30)", "line(d,60,30,60,60)",
    "line(d,60,60,30,60)","line(d,30,60,30,30)", NULL
};

// The calls that should be made for box.sketch.
char **boxTest = (char *[]) {
    "line(d,30,30,32,30)", "pause(d,10)", "line(d,32,30,34,30)", "pause(d,10)",
    "line(d,34,30,36,30)", "pause(d,10)", "line(d,36,30,38,30)", "pause(d,10)",
    "line(d,38,30,40,30)", "pause(d,10)", "line(d,40,30,42,30)", "pause(d,10)",
    "line(d,42,30,44,30)", "pause(d,10)", "line(d,44,30,46,30)", "pause(d,10)",
    "line(d,46,30,48,30)", "pause(d,10)", "line(d,48,30,50,30)", "pause(d,10)",
    "line(d,50,30,52,30)", "pause(d,10)", "line(d,52,30,54,30)", "pause(d,10)",
    "line(d,54,30,56,30)", "pause(d,10)", "line(d,56,30,58,30)", "pause(d,10)",
    "line(d,58,30,60,30)", "pause(d,10)", "line(d,60,30,60,32)", "pause(d,10)",
    "line(d,60,32,60,34)", "pause(d,10)", "line(d,60,34,60,36)", "pause(d,10)",
    "line(d,60,36,60,38)", "pause(d,10)", "line(d,60,38,60,40)", "pause(d,10)",
    "line(d,60,40,60,42)", "pause(d,10)", "line(d,60,42,60,44)", "pause(d,10)",
    "line(d,60,44,60,46)", "pause(d,10)", "line(d,60,46,60,48)", "pause(d,10)",
    "line(d,60,48,60,50)", "pause(d,10)", "line(d,60,50,60,52)", "pause(d,10)",
    "line(d,60,52,60,54)", "pause(d,10)", "line(d,60,54,60,56)", "pause(d,10)",
    "line(d,60,56,60,58)", "pause(d,10)", "line(d,60,58,60,60)", "pause(d,10)",
    "line(d,60,60,58,60)", "pause(d,10)", "line(d,58,60,56,60)", "pause(d,10)",
    "line(d,56,60,54,60)", "pause(d,10)", "line(d,54,60,52,60)", "pause(d,10)",
    "line(d,52,60,50,60)", "pause(d,10)", "line(d,50,60,48,60)", "pause(d,10)",
    "line(d,48,60,46,60)", "pause(d,10)", "line(d,46,60,44,60)", "pause(d,10)",
    "line(d,44,60,42,60)", "pause(d,10)", "line(d,42,60,40,60)", "pause(d,10)",
    "line(d,40,60,38,60)", "pause(d,10)", "line(d,38,60,36,60)", "pause(d,10)",
    "line(d,36,60,34,60)", "pause(d,10)", "line(d,34,60,32,60)", "pause(d,10)",
    "line(d,32,60,30,60)", "pause(d,10)", "line(d,30,60,30,58)", "pause(d,10)",
    "line(d,30,58,30,56)", "pause(d,10)", "line(d,30,56,30,54)", "pause(d,10)",
    "line(d,30,54,30,52)", "pause(d,10)", "line(d,30,52,30,50)", "pause(d,10)",
    "line(d,30,50,30,48)", "pause(d,10)", "line(d,30,48,30,46)", "pause(d,10)",
    "line(d,30,46,30,44)", "pause(d,10)", "line(d,30,44,30,42)", "pause(d,10)",
    "line(d,30,42,30,40)", "pause(d,10)", "line(d,30,40,30,38)", "pause(d,10)",
    "line(d,30,38,30,36)", "pause(d,10)", "line(d,30,36,30,34)", "pause(d,10)",
    "line(d,30,34,30,32)", "pause(d,10)", "line(d,30,32,30,30)", "pause(d,10)",
    NULL
};

// The calls that should be made for box.sketch.
char **oxoTest = (char *[]) {
    "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)",
    "line(d,30,40,60,40)",
    "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)",
    "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)",
    "line(d,30,50,60,50)",
    "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)",
    "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)",
    "line(d,40,30,40,60)",
    "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)",
    "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)",
    "line(d,50,30,50,60)", NULL
};

// The calls that should be made for diag.sketch.
char **diagTest = (char *[]) {
    "line(d,30,30,60,60)", NULL
};

// The calls that should be made for cross.sketch.
char **crossTest = (char *[]) {
    "line(d,30,30,60,60)", "line(d,60,30,30,60)", NULL
};

// The calls that should be made for clear.sketch.
char **clearTest = (char *[]) {
    "line(d,30,40,60,40)", "line(d,30,50,60,50)", "line(d,40,30,40,60)",
    "line(d,50,30,50,60)", "pause(d,63)", "pause(d,63)", "pause(d,63)",
    "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)",
    "pause(d,63)", "pause(d,63)", "clear(d)", "line(d,30,30,60,60)",
    "line(d,60,30,30,60)", NULL
};

// The calls that should be made for key.sketch.
char **keyTest = (char *[]) {
    "line(d,30,40,60,40)", "line(d,30,50,60,50)", "line(d,40,30,40,60)",
    "line(d,50,30,50,60)", "pause(d,63)", "pause(d,63)", "pause(d,63)",
    "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)", "pause(d,63)",
    "pause(d,63)", "pause(d,63)", "key(d)", "clear(d)", "line(d,30,30,60,60)",
    "line(d,60,30,30,60)", NULL
};

// The calls that should be made for diag.sketch.
char **pausesTest = (char *[]) {
    "pause(d,0)", "pause(d,0)", "pause(d,127)", "pause(d,128)", "pause(d,300)",
    "pause(d,0)", "pause(d,71469)", NULL
};

// The calls that should be made for field.sketch.
char **fieldTest = (char *[]) {
    "line(d,30,30,170,30)", "line(d,170,30,170,170)",
    "line(d,170,170,30,170)", "line(d,30,170,30,30)", NULL
};

// The calls that should be made for field.sketch.
char **lawnTest = (char *[]) {
    "cline(d,30,30,170,30,16711935)", "cline(d,170,30,170,170,16711935)",
    "cline(d,170,170,30,170,16711935)", "cline(d,30,170,30,30,16711935)",
    NULL
};

char **extLawnTest = (char *[])
{
  "rect(d,44,30,30,120,1569195464)", "rect(d,74,30,30,120,-1968029752)",
  "rect(d,104,30,30,120,1569195464)", "rect(d,134,30,30,120,-1968029752)",
  "rect(d,164,30,30,120,1569195464)", NULL
};
